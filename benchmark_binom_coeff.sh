#!/bin/bash

echo "execute binom_coeff_loops(100,4)"
gcc binom_coeff_loops.c -o binom_coeff_loops.out
time ./binom_coeff_loops.out 100 4
echo "execute binom_coeff_recursion(100,4)"
gcc binom_coeff_recursion.c -o binom_coeff_recursion.out
time ./binom_coeff_recursion.out 100 4

echo "execute binom_coeff_loops(50,25)"
gcc binom_coeff_loops.c -o binom_coeff_loops.out
time ./binom_coeff_loops.out 50 25
echo "execute binom_coeff_recursion(50,25)"
gcc binom_coeff_recursion.c -o binom_coeff_recursion.out
time ./binom_coeff_recursion.out 50 25
