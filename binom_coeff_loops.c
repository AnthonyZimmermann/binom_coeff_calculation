#include <stdio.h>
#include <stdlib.h>

#define ITERATIONS 100000000

unsigned long falling_factorial(unsigned int n, unsigned int k)
{
    unsigned long res = 1;
    int i;
    for (i=n; i>k; i--)
        res *= i;
    return res;
}

unsigned long binom_coeff(unsigned int n, unsigned int k)
{
    return falling_factorial(n,n-k)/falling_factorial(k,0);
}

int main(int argc, char** argv)
{
    int n = atoi(argv[1]);
    int k = atoi(argv[2]);

    printf("binom_coeff_loops(n=%d,k=%d) = %lu\n", n, k, binom_coeff(n,k));

    printf("execute binom_coeff_loops(n=%d,k=%d) %d times:\n", n, k, ITERATIONS);
    unsigned int i;
    unsigned long res = 0;
    for (i=0; i<ITERATIONS; i++)
    {
        // addition is necessary for the quick&easy benchmark
        // -> prevent that compiler removes any number of iterations
        // -> because the computation results are not used anywhere
        // -> afterwards
        res += binom_coeff(n,k);
    }
    printf("dummy print, to avoid compiler optimisations %d", res);
}
