binomial coefficient(n,k) = n! / ( k! * (n-k)! )

with k<n:
n! = n * (n-1) * (n-2) * ... * (n-(k+1)) * (n-k)!

n! / [ k! * (n-k)! ]

= [ n * (n-1) * (n-2) * ... * (n-(k+1)) * (n-k)! ] / [ k! * (n-k)! ]

= [ n * (n-1) * (n-2) * ... * (n-(k+1)) ] / [ k! ]
