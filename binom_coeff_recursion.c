#include <stdio.h>
#include <stdlib.h>

#define ITERATIONS 100000000

unsigned long falling_factorial(unsigned int n, unsigned int k)
{
    if ( n == k+1)
        return n;
    return n*falling_factorial(n-1,k);
}

unsigned long binom_coeff(unsigned int n, unsigned int k)
{
    return falling_factorial(n,n-k)/falling_factorial(k,1);
}

int main(int argc, char** argv)
{
    int n = atoi(argv[1]);
    int k = atoi(argv[2]);

    printf("binom_coeff_recursion(n=%d,k=%d) = %lu\n", n, k, binom_coeff(n,k));

    printf("execute binom_coeff_recursion(n=%d,k=%d) %d times:\n", n, k, ITERATIONS);
    unsigned int i;
    unsigned long res = 0;
    for (i=0; i<ITERATIONS; i++)
    {
        // addition is necessary for the quick&easy benchmark
        // -> prevent that compiler removes any number of iterations
        // -> because the computation results are not used anywhere
        // -> afterwards
        res += binom_coeff(n,k);
    }
    printf("dummy print, to avoid compiler optimisations %d", res);
}
